2010-04-22  Alan Modra  <amodra@gmail.com>

	* ld-elf/extract-symbol-1sec.d: Update lma.
	* ld-i386/alloc.d: Expect a warning, not an error.

2010-04-20  Joseph Myers  <joseph@codesourcery.com>

	* ld-tic6x/data-reloc-global-rel.d,
	ld-tic6x/data-reloc-global-rel.s,
	ld-tic6x/data-reloc-local-r-rel.d,
	ld-tic6x/data-reloc-local-rel.d, ld-tic6x/mvk-reloc-global-rel.d,
	ld-tic6x/mvk-reloc-global-rel.s, ld-tic6x/mvk-reloc-local-1-rel.s,
	ld-tic6x/mvk-reloc-local-2-rel.s,
	ld-tic6x/mvk-reloc-local-r-rel.d, ld-tic6x/mvk-reloc-local-rel.d,
	ld-tic6x/pcrel-reloc-global-rel.d,
	ld-tic6x/pcrel-reloc-local-r-rel.d,
	ld-tic6x/pcrel-reloc-local-rel.d, ld-tic6x/sbr-reloc-global-rel.d,
	ld-tic6x/sbr-reloc-global-rel.s, ld-tic6x/sbr-reloc-local-1-rel.s,
	ld-tic6x/sbr-reloc-local-2-rel.s,
	ld-tic6x/sbr-reloc-local-r-rel.d, ld-tic6x/sbr-reloc-local-rel.d:
	New.

2010-04-15  Matthew Gretton-Dann  <matthew.gretton-dann@arm.com>

	* ld-arm/attr-merge-2.attr: Update for changes in attribute output.
	* ld-arm/attr-merge-3.attr: Likewise.
	* ld-arm/attr-merge-vfp-1.d: Likewise.
	* ld-arm/attr-merge-vfp-1r.d: Likewise.
	* ld-arm/attr-merge-vfp-2.d: Likewise.
	* ld-arm/attr-merge-vfp-2r.d: Likewise.
	* ld-arm/attr-merge-vfp-3.d: Likewise.
	* ld-arm/attr-merge-vfp-3r.d: Likewise.
	* ld-arm/attr-merge-vfp-4.d: Likeiwse.
	* ld-arm/attr-merge-vfp-4r.d: Likewise.
	* ld-arm/attr-merge-vfp-5.d: Likewise.
	* ld-arm/attr-merge-vfp-5r.d: Likewise.
	* ld-arm/attr-merge-wchar-00-nowarn.d: Likewise.
	* ld-arm/attr-merge-wchar-00.d: Likewise.
	* ld-arm/attr-merge-wchar-02-nowarn.d: Likewise.
	* ld-arm/attr-merge-wchar-02.d: Likewise.
	* ld-arm/attr-merge-wchar-04-nowarn.d: Likewise.
	* ld-arm/attr-merge-wchar-04.d: Likewise.
	* ld-arm/attr-merge-wchar-20-nowarn.d: Likewise.
	* ld-arm/attr-merge-wchar-20.d: Likewise.
	* ld-arm/attr-merge-wchar-22-nowarn.d: Likewise.
	* ld-arm/attr-merge-wchar-22.d: Likewise.
	* ld-arm/attr-merge-wchar-24-nowarn.d: Likewise.
	* ld-arm/attr-merge-wchar-40-nowarn.d: Likewise.
	* ld-arm/attr-merge-wchar-40.d: Likewise.
	* ld-arm/attr-merge-wchar-42-nowarn.d: Likewise.
	* ld-arm/attr-merge-wchar-44-nowarn.d: Likewise.
	* ld-arm/attr-merge-wchar-44.d: Likewise.
	* ld-arm/attr-merge.attr: Likewise.

2010-04-06  David S. Miller  <davem@davemloft.net>

	* ld-elfvers/vers.exp: Pass -Av9a to assembler on sparc-*-*

2010-04-05  Kai Tietz  <kai.tietz@onevision.com>

	* ld-pe/orphan_nu.d: New test for --no-leading-underscore.
	* ld-pe/orphana_nu.s: New file.
	* ld-pe/pe.exp: Add orphan_nu test.

2010-04-01  Nathan Sidwell  <nathan@codesourcery.com>

	* ld-powerpc/apuinfo-nul.rd: New.
	* ld-powerpc/apuinfo-nul1.s: New.
	* ld-powerpc/powerpc.exp: Add it.

2010-03-31  Kai TIetz  <kai.tietz@onevision.com>

	* ld-pe//pe-compile.exp (run_basefile_test): Trim result of wc
	before string compare.

2010-03-31  Matthew Gretton-Dann  <matthew.gretton-dann@arm.com>

	* ld-arm/script-type.sym: Fix test.

2010-03-31  Kai TIetz  <kai.tietz@onevision.com>

	* ld-pe/basefile1.s: New.
	* ld-pe/pe-compile.exp: Add base-file test.

2010-03-31  Hans-Peter Nilsson  <hp@axis.com>

	PR ld/11458
	* ld-cris/pcrelcp-1.d, ld-cris/pcrelcp-1.s: New test.

	* lib/ld-lib.exp (run_dump_test): When checking linker message and
	return code, when success with no message is expected, don't
	continue if we have an abnormal exit with a message.  Check output
	of inspection program and fail if it had output or an abnormal
	exit code.  Include "warning" and "error" among the directives
	where multiples are allowed and append to previous values.

	* ld-cris/cris.exp (loop over $srcdir/$subdir/*dso-*.d): Apply
	"file rootname", not "file tail", before applying runtest_file_p.

2010-03-29  Daniel Jacobowitz  <dan@codesourcery.com>

	* ld-arm/arm-elf.exp (armeabitests): Add v6-M farcall test.

2010-03-25  Joseph Myers  <joseph@codesourcery.com>

	* ld-elf/flags1.d, ld-elf/merge.d: XFAIL for tic6x-*-*.
	* ld-elf/sec-to-seg.exp: Set B_test_same_seg to 0 for tic6x-*-*.
	* ld-tic6x: New directory and testcases.

2010-03-19  Jie Zhang  <jie@codesourcery.com>

	PR ld/11304
	* ld-elf/pr11304.d: New test.
	* ld-elf/pr11304a.s: New test.
	* ld-elf/pr11304b.s: New test.
	* lib/ld-lib.exp (regexp_diff): Add support for #failif.

2010-03-15  Daniel Jacobowitz  <dan@codesourcery.com>

	* ld-elf/orphan-region.d, ld-elf/orphan-region.ld,
	ld-elf/orphan-region.s: New files.

2010-03-02  Matthew Gretton-Dann  <matthew.gretton-dann@arm.com>

	* ld-arm/arm-merge-incompatible.d: New test.
	* ld-arm/arm-merge-incompatiblea.s: Likewise.
	* ld-arm/arm-merge-incompatibleb.s: Likewise.
	* ld-arm/arm-elf.exp: Run the new test.

2010-03-02  Christophe Lyon  <christophe.lyon@st.com>
	    Alan Modra  <amodra@gmail.com>

	* ld-arm/arm-elf.exp: Change .text start address for
	farcall-thumb-arm tests. Add v4t variant for farcall-mixed-lib
	test.
	* ld-arm/farcall-mixed-lib-v4t.d: New test.
	* ld-arm/farcall-mixed-lib1.s: Don't force armv5t.
	* ld-arm/farcall-mixed-lib2.s: Likewise.
	* ld-arm/arm-call.d: Update expected results.
	* ld-arm/cortex-a8-far.d: Likewise.
	* ld-arm/farcall-group-size2.d: Likewise.
	* ld-arm/farcall-group.d: Likewise.
	* ld-arm/farcall-mix.d: Likewise.
	* ld-arm/farcall-mix2.d: Likewise.
	* ld-arm/farcall-mixed-app-v5.d: Likewise.
	* ld-arm/farcall-mixed-app.d: Likewise.
	* ld-arm/farcall-mixed-lib.d: Likewise.
	* ld-arm/farcall-thumb-arm.d: Likewise.
	* ld-arm/farcall-thumb-arm-blx.d: Likewise.
	* ld-arm/farcall-thumb-arm-pic-veneer.d: Likewise.
	* ld-arm/farcall-thumb-arm-blx-pic-veneer.d: Likewise.
	* ld-arm/farcall-thumb-arm.s: Update test. Add a new call to
	potentially generate different types of stubs.

2010-02-27  H.J. Lu  <hongjiu.lu@intel.com>

	* ld-elf/init-fini-arrays.d: Pass --wide to readelf.

2010-02-27  Jie Zhang  <jie@codesourcery.com>

	* ld-elf/init-fini-arrays.s: New test.
	* ld-elf/init-fini-arrays.d: New test.

2010-02-24  Matthew Gretton-Dann  <matthew.gretton-dann@arm.com>

	* ld-arm/group-relocs.s: Mark code sections as executable.
	* ld-arm/arm-elf.exp (armelftests): Only dump executable sections in
	group-relocs test.
	* ld-arm/reloc-boundaries.d: Fix test to work on Linux targets.

2010-02-23  Nick Clifton  <nickc@redhat.com>

	* ld-elf/orphan4.d: Allow for other sections to be present in the
	output.

2010-02-18  H.J. Lu  <hongjiu.lu@intel.com>

	* ld-ifunc/ifunc.exp: Expect System V OSABI in dynamic
	ifunc-using executable.

2010-02-19  Alan Modra  <amodra@gmail.com>

	* ld-elf/group.ld: Discard .dropme sections.
	* ld-elf/group10.d, * ld-elf/group10.s: New test.

2010-02-18  Matthew Gretton-Dann  <matthew.gretton-dann@arm.com>

	* ld-arm/attr-merge-6.attr: Add new test.  Missed off last commit.

2010-02-18  Matthew Gretton-Dann  <matthew.gretton-dann@arm.com>

	* ld-arm/attr-merge-3.attr: Fix test for new attribute values.
	* ld-arm/attr-merge-3b.s: Likewise.
	* ld-arm/attr-merge-unknown-1.d: Fix test now that 42 is a recognised
	attribute ID.
	* ld-arm/attr-merge-unknown-1.s: Likewise.
	* ld-arm/attr-merge-6.attr: New test.
	* ld-arm/attr-merge-6a.s: Likewise.
	* ld-arm/attr-merge-6b.s: Likewise.
	* ld-arm/attr-merge-7.attr: Likewise.
	* ld-arm/attr-merge-7a.s: Likewise.
	* ld-arm/attr-merge-7b.s: Likewise.
	* ld-arm/arm-elf.exp: Run the new tests.

2010-02-15  Matthew Gretton-Dann <matthew.gretton-dann@arm.com>

	* ld-arm/jump-reloc-veneers-long.d: Correct testcase for
	  arm-none-eabi target.
	* ld-arm/jump-reloc-veneers-short1.d: Likewise
	* ld-arm/jump-reloc-veneers-short2.d: Likewise

2010-02-12  Daniel Gutson  <dgutson@codesourcery.com>

	* ld-arm/arm-elf.exp (armelftests): New test case added.
	* ld-arm/data-only-map.s: New file.
	* ld-arm/data-only-map.d: New file.
	* ld-arm/data-only-map.ld: New file.

2010-02-11  David S. Miller  <davem@davemloft.net>

	* ld-sparc/gotop32.s: Add local symbol case.
	* ld-sparc/gotop64.s: Likewise.
	* ld-sparc/gotop32.rd: Adjust expected results.
	* ld-sparc/gotop32.td: Likewise.
	* ld-sparc/gotop64.dd: Likewise.
	* ld-sparc/gotop64.rd: Likewise.
	* ld-sparc/gotop64.td: Likewise.

2010-02-09  Matthew Gretton-Dann  <matthew.gretton-dann@arm.com>

	* ld-elfvsb/elfvsb.exp: Fix tests for arm*-*-linux*.
	* ld-shared/shared.exp: Likewise.

2010-02-08  David S. Miller  <davem@davemloft.net>

	* ld-ifunc/ifunc.exp: Run for sparc.

2010-02-08  Nathan Sidwell  <nathan@codesourcery.com>

	* ld-powerpc/apuinfo-nul.s: New.
	* ld-powerpc/apuinfo.rd: Add it.
	* ld-powerpc/powerpc.exp: Likewise.

2010-02-01  Matthew Gretton-Dann  <matthew.gretton-dann@arm.com>

	* ld-arm/jump-reloc-veneers-long.d: New test.
	* ld-arm/jump-reloc-veneers-short1.d: Likewise.
	* ld-arm/jump-reloc-veneers-short2.d: Likewise.
	* ld-arm/jump-reloc-veneers.s: Likewise.
	* ld-arm/arm-elf.exp (armelftests): Run them.

2010-01-28  Nick Clifton  <nickc@redhat.com>

	PR 11225
	* ld-sh/refdbg-0-dso.d: Dump all sections.

2010-01-26  H.J. Lu  <hongjiu.lu@intel.com>

	PR ld/11218
	* ld-gc/dummy.s: New.
	* ld-gc/pr11218-1.c: Likewise.
	* ld-gc/pr11218-2.c: Likewise.
	* ld-gc/pr11218.d: Likewise.

2010-01-23  Richard Sandiford  <r.sandiford@uk.ibm.com>

	* ld-powerpc/aix-ref-1-32.od, ld-powerpc/aix-ref-1-64.od,
	ld-powerpc/aix-ref-1.s: New tests.
	* ld-powerpc/aix52.exp: Run them.

2010-01-14  H.J. Lu  <hongjiu.lu@intel.com>

	* ld-elf/orphan4.d: Support 64bit targets.

2010-01-13  DJ Delorie  <dj@redhat.com>

	* ld-elf/orphan4.d: New.
	* ld-elf/orphan4.ld: New.
	* ld-elf/orphan4.s: New.

2010-01-13  Chao-ying Fu  <fu@mips.com>

	* ld-mips-elf/jr-to-b-1.d, ld-mips-elf/jr-to-b-2.d: New tests.
	* ld-mips-elf/jr-to-b-1.s, ld-mips-elf/jr-to-b-2.s: Source.
	* ld-mips-elf/mips-elf.exp: Run new tests.

2010-01-13  Daniel Jacobowitz  <dan@codesourcery.com>

	* ld-arm/arm-elf.exp (armelftests): Assemble Cortex-A8 tests with
	-mcpu=cortex-a8.

2010-01-13  Nick Clifton  <nickc@redhat.com>

	* ld-scrips/sort.exp: Skip these tests when the target is the
	h8300.

2010-01-11  H.J. Lu  <hongjiu.lu@intel.com>

	PR ld/11146
	* ld-elf/dynsym1.d: New.

2010-01-07  H.J. Lu  <hongjiu.lu@intel.com>

	PR ld/11138
	* ld-elf/pr11138-1.c: New.
	* ld-elf/pr11138-1.map: Likewise.
	* ld-elf/pr11138-2.c: Likewise.
	* ld-elf/pr11138-2.map: Likewise.
	* ld-elf/pr11138.out: Likewise.

	* ld-elf/shared.exp (build_tests): Add libpr11138-1.so and
	libpr11138-2.o.
	(run_tests): Add 2 tests for PR ld/11138.

2010-01-07  H.J. Lu  <hongjiu.lu@intel.com>

	PR ld/11133
	* ld-gc/gc.exp: Run start.

	* ld-gc/start.d: New.
	* ld-gc/start.s: Likewise.

2010-01-07  H.J. Lu  <hongjiu.lu@intel.com>

	PR ld/11143
	* ld-gc/gc.exp: Run abi-note.

	* ld-gc/abi-note.d: New.
	* ld-gc/abi-note.s: Likewise.

For older changes see ChangeLog-2009

Local Variables:
mode: change-log
left-margin: 8
fill-column: 74
version-control: never
End:
